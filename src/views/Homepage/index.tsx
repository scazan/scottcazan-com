import React from 'react';
import { Store, getEvents } from '../../store/Store';
import './index.scss';

const Homepage: React.FC = () => {
  const { state, dispatch } = React.useContext(Store);
  console.log(state);

  React.useEffect(() => {
    state.events.length === 0 && getEvents(dispatch);
  });

  return (
    <div className="homepage">
      <header>
        {state.events.map((event: any) => event.title[0].text)}
      </header>
    </div>
  );
}

export default Homepage;
