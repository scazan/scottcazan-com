import * as React from 'react';
import { PrismicLink } from 'apollo-link-prismic';
import { InMemoryCache } from "apollo-cache-inmemory";
import ApolloClient from "apollo-client";
import gql from "graphql-tag";

const client = new ApolloClient({
  link: PrismicLink({
    uri: "https://scottcazan.prismic.io/graphql",
  }),
  cache: new InMemoryCache()
});

export interface IStore {
  events: any[],
  works: any[],
};

export const getEvents = async (dispatch) => {
  const events = await client.query({
    query: gql`
      query{
        allEvents(lang:"en-us"){
          edges {
            node {
              venue
              title
            }
          }
        }
      }
    `,
  });

  const payload = events.data.allEvents.edges.map(event => event.node);

  return dispatch({
    type: 'FETCH_EVENTS',
    payload,
  });
};

const initialState: IStore = {
  events: [],
  works: [],
};

export const Store = React.createContext({state: initialState, dispatch: () => {}});


const reducer = (state: IStore, action) => {
  switch (action.type) {
    case 'FETCH_EVENTS':
      return {...state, events: action.payload};
    default:
      return state;
  }
};

export const EventsProvider = ( props ) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const value: { state, dispatch: any} = { state, dispatch };

  return <Store.Provider value={value}>
    {props.children}
  </Store.Provider>;
};
